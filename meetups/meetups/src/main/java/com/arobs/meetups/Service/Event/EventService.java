package com.arobs.meetups.Service.Event;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EventService {
    void addEvent(EventDTO eventDTO);
    List<EventDTO> getAllEvents();
    void deleteEvent(int event_id);
    void editEvent(EventDTO eventDTO);
    int getMaxParticipants(int event_id);
    String closeEvent(int event_id);
    EventDTO findById(int event_id);
}
