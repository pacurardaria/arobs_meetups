package com.arobs.meetups.Service.User;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    void addUser(UserDTO u);
    List<UserDTO> getAllUsers();
    void deleteUser(int user_id);
    void editUser(UserDTO userDTO);
    void addPointsForUserID(int points, int user_id);
    void clearPoints();
}
