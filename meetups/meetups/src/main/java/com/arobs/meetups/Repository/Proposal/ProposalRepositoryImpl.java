package com.arobs.meetups.Repository.Proposal;

import com.arobs.meetups.Entities.Proposal;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


@Repository
public class ProposalRepositoryImpl implements ProposalRepository {
    @Autowired
    private SessionFactory sessionFactory;

    public void addProposal(Proposal proposal) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(proposal);
    }

    @Override
    public void deleteProposal(int proposal_id) {
        Session currentSession = sessionFactory.getCurrentSession();
        Proposal proposal = new Proposal();
        proposal.setProposal_id(proposal_id);
        currentSession.delete(proposal);
    }

    @Override
    public void editProposal(Proposal proposal) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.update(proposal);
    }

    @Override
    public List<Proposal> getAllProposals() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Proposal> cq = cb.createQuery(Proposal.class);
        Root<Proposal> root = cq.from(Proposal.class);
        cq.select(root);
        Query query = session.createQuery(cq);
        return query.getResultList();
    }



}
