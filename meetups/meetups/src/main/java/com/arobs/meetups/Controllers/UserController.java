package com.arobs.meetups.Controllers;

import com.arobs.meetups.Service.User.UserDTO;
import com.arobs.meetups.Service.User.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@Api(value = "Users controller")
public class UserController {

    @Autowired
    UserService userService;

    @ApiOperation(value = "Save user")
    @PostMapping("/add")
    public ResponseEntity<UserDTO> addUser(@RequestBody UserDTO userDTO) {
        userService.addUser(userDTO);
        return ResponseEntity.ok(userDTO);
    }

    @ApiOperation(value = "Delete user")
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable int user_id) {
        userService.deleteUser(user_id);
        return ResponseEntity.ok("Deleted");
    }

    @ApiOperation(value = "Edit user")
    @PutMapping("/edit")
    public ResponseEntity<UserDTO> editUser(@RequestBody UserDTO userDTO) {
        userService.editUser(userDTO);
        return ResponseEntity.ok(userDTO);
    }

    @ApiOperation(value = "See all users")
    @GetMapping(path = "/all")
    public ResponseEntity<List<UserDTO>> getAll() {
        return ResponseEntity.ok(userService.getAllUsers());
    }


    @ApiOperation(value = "Add points to user")
    @PutMapping("/addPoints/{points}/{user_id}")
    public ResponseEntity<String> addPoints(@PathVariable int points, @PathVariable int user_id) {
        userService.addPointsForUserID(points, user_id);
        return ResponseEntity.ok("Added points to user");
    }

    @ApiOperation(value = "Clear points")
    @PutMapping("/clearPoints")
    public ResponseEntity<String> clearPoints() {
        userService.clearPoints();
        return ResponseEntity.ok("Cleared points to all users");
    }
}
