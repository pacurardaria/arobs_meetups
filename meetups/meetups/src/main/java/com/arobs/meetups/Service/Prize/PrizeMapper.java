package com.arobs.meetups.Service.Prize;

import com.arobs.meetups.Entities.Prize;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class PrizeMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Prize.class, PrizeDTO.class)
                .byDefault()
                .register();
    }
}

