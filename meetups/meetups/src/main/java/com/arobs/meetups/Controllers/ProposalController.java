package com.arobs.meetups.Controllers;

import com.arobs.meetups.Service.Proposal.ProposalDTO;
import com.arobs.meetups.Service.Proposal.ProposalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/proposals")
@Api(value = "Proposals controller")
public class ProposalController {

    @Autowired
    ProposalService proposalService;

    @ApiOperation(value = "See all proposals")
    @GetMapping(path = "/all")
    public ResponseEntity<List<ProposalDTO>> getAll() {
        return ResponseEntity.ok(proposalService.getAllProposals());
    }

    @ApiOperation(value = "Add new proposal")
    @PostMapping("/add")
    public ResponseEntity<ProposalDTO> addProposal(@RequestBody ProposalDTO proposalDTO) {
        proposalService.addProposal(proposalDTO);
        return ResponseEntity.ok(proposalDTO);
    }

    @ApiOperation(value = "Edit proposal")
    @PutMapping("/edit")
    public ResponseEntity<ProposalDTO> editProposal(@RequestBody ProposalDTO proposalDTO) {
        proposalService.editProposal(proposalDTO);
        return ResponseEntity.ok(proposalDTO);
    }

    @ApiOperation(value = "Delete proposal")
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<String> deleteProposal(@PathVariable int proposal_id) {
        proposalService.deleteProposal(proposal_id);
        return ResponseEntity.ok("Deleted");
    }

}
