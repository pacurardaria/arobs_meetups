package com.arobs.meetups.Repository.Prize;

import com.arobs.meetups.Entities.Prize;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class PrizeRepositoryImpl implements PrizeRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addPrize(Prize prize) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(prize);
    }

    @Override
    public void editPrize(Prize prize) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.update(prize);
    }

    @Override
    public void deletePrize(int prize_id) {
        Session currentSession = sessionFactory.getCurrentSession();
        Prize prize = new Prize();
        prize.setPrize_id(prize_id);
        currentSession.delete(prize);
    }

    @Override
    public List<Prize> getAllPrizes() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Prize> cq = cb.createQuery(Prize.class);
        Root<Prize> root = cq.from(Prize.class);
        cq.select(root);
        Query query = session.createQuery(cq);
        return query.getResultList();
    }
}
