package com.arobs.meetups.Entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "awarding_history")
public class AwardingHistory {
    @Id
    @GeneratedValue(
            strategy= GenerationType.AUTO,
            generator="native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private int awarding_id;
    private Date awarding_date;
    private int number_of_points;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "prize_id", nullable = false)
    private Prize prize;

    public int getAwarding_id() {
        return awarding_id;
    }

    public void setAwarding_id(int awarding_id) {
        this.awarding_id = awarding_id;
    }

    public Date getAwarding_date() {
        return awarding_date;
    }

    public void setAwarding_date(Date awarding_date) {
        this.awarding_date = awarding_date;
    }

    public int getNumber_of_points() {
        return number_of_points;
    }

    public void setNumber_of_points(int number_of_points) {
        this.number_of_points = number_of_points;
    }
}
