package com.arobs.meetups.Service.Vote;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface VoteService {
    void addVotes(VoteDTO voteDTO);
    int countVotesForProposal(int proposal_id);
    List<VoteDTO> getAll();
    List<VotedProposalsDTO> getTopProposals();
}
