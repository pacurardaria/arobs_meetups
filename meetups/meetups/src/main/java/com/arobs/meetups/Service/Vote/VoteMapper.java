package com.arobs.meetups.Service.Vote;

import com.arobs.meetups.Entities.Vote;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class VoteMapper extends ConfigurableMapper {
        @Override
        protected void configure(MapperFactory factory) {
            factory.classMap(Vote.class, VoteDTO.class)
                    .byDefault()
                    .register();
        }
}
