package com.arobs.meetups.Repository.Votes;

import com.arobs.meetups.Entities.Proposal;
import com.arobs.meetups.Entities.Vote;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class VoteRepositoryImpl implements VoteRepository {
    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void addVotes(Vote vote) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(vote);
    }

    @Override
    public int countVotesForProposal(int proposal_id) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Vote> cr = cb.createQuery(Vote.class);
        Root<Vote> root = cr.from(Vote.class);
        cr.select(root).where(cb.equal(root.get("proposal").get("proposal_id"), proposal_id));

        Query<Vote> query = session.createQuery(cr);
        List<Vote> results = query.getResultList();
        return results.size();
    }

    @Override
    public List<Vote> getAll() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Vote> cq = cb.createQuery(Vote.class);
        Root<Vote> root = cq.from(Vote.class);
        cq.select(root);
        Query query = session.createQuery(cq);
        return query.getResultList();
    }

}
