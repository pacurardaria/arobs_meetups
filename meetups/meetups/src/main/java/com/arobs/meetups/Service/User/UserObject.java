package com.arobs.meetups.Service.User;

import com.arobs.meetups.Entities.User;
import com.arobs.meetups.Repository.Event.EventRepository;
import com.arobs.meetups.Repository.User.UserRepository;
import com.arobs.meetups.Repository.User.UserRepositoryConstants;
import com.arobs.meetups.Repository.User.UserRepositoryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserObject {

    @Autowired
    UserRepositoryFactory userRepositoryFactory;

    @Autowired
    UserMapper userMapper;

    @Autowired
    EventRepository eventRepository;

    public void saveUser(UserDTO userDTO) {
        UserRepository userRepository = userRepositoryFactory.createUserRepository(UserRepositoryConstants.HIBERNATE_REPOSITORY);
        if (userDTO!=null){
            User user = userMapper.map(userDTO, User.class);
            userRepository.addUser(user);
        }
    }

    public List<UserDTO> getAllUsers(){
        UserRepository userRepository = userRepositoryFactory.createUserRepository(UserRepositoryConstants.HIBERNATE_REPOSITORY);
        List<User> userList = userRepository.getAllUsers();
        if (!userList.isEmpty()) {
            return userMapper.mapAsList(userList, UserDTO.class);
        }
        return null;
    }


    public void deleteUser(int user_id) {
        UserRepository userRepository = userRepositoryFactory.createUserRepository(UserRepositoryConstants.HIBERNATE_REPOSITORY);
        userRepository.deleteUser(user_id);

    }

    public void editUser(UserDTO userDTO) {
        UserRepository userRepository = userRepositoryFactory.createUserRepository(UserRepositoryConstants.HIBERNATE_REPOSITORY);
        if (userDTO != null) {
            User user = userMapper.map(userDTO, User.class);
            userRepository.editUser(user);
        }
    }

    public void addPointsForUserID(int pointsToAdd, int user_id) {
        UserRepository userRepository = userRepositoryFactory.createUserRepository(UserRepositoryConstants.JDBC_REPOSITORY);
        userRepository.addPointsForUserID(pointsToAdd, user_id);
    }

    public void clearPoints() {
        UserRepository userRepository = userRepositoryFactory.createUserRepository(UserRepositoryConstants.JDBC_REPOSITORY);
        userRepository.clearPoints();
    }
}
