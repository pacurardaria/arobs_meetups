package com.arobs.meetups.Service.Vote;

import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.text.html.parser.Entity;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class VoteServiceImpl implements VoteService {
    @Autowired
    VoteObject voteObject;

    @Override
    @Transactional
    public void addVotes(VoteDTO voteDTO) {
        voteObject.addVotes(voteDTO);
    }

    @Override
    @Transactional
    public int countVotesForProposal(int proposal_id) {
        return voteObject.getVotesForProposal(proposal_id);
    }

    @Override
    @Transactional
    public List<VoteDTO> getAll() {
        return voteObject.getAll();
    }

    @Override
    @Transactional
    public List<VotedProposalsDTO> getTopProposals() {
        return voteObject.getTopProposals();
    }

}
