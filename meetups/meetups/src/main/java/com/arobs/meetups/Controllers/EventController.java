package com.arobs.meetups.Controllers;

import com.arobs.meetups.Entities.Event;
import com.arobs.meetups.Service.Event.EventDTO;
import com.arobs.meetups.Service.Event.EventService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/events")
@Api(value = "Events controller")
public class EventController {
    @Autowired
    EventService eventService;

    @ApiOperation(value = "Add new event")
    @PostMapping("/add")
    public ResponseEntity<EventDTO> addEvent(@RequestBody EventDTO eventDTO) {
        eventService.addEvent(eventDTO);
        return ResponseEntity.ok(eventDTO);
    }

    @ApiOperation(value = "Edit event")
    @PutMapping("/edit")
    public ResponseEntity<EventDTO> editEvent(@RequestBody EventDTO eventDTO) {
        eventService.editEvent(eventDTO);
        return ResponseEntity.ok(eventDTO);
    }

    @ApiOperation(value = "Delete event")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteEvent(@PathVariable int id) {
        eventService.deleteEvent(id);
        return ResponseEntity.ok("Deleted");
    }

    @ApiOperation(value = "See all events")
    @GetMapping(path = "/all")
    public ResponseEntity<List<EventDTO>> getAll() {
        return ResponseEntity.ok(eventService.getAllEvents());
    }



    @ApiOperation(value="Get max no of participants")
    @GetMapping(path="/maxparticipants/{id}")
    public ResponseEntity<Integer> getMaxParticipants(@PathVariable int id) {
        return ResponseEntity.ok(eventService.getMaxParticipants(id));
    }

    @ApiOperation(value = "Close event")
    @PutMapping("/closeEvent/{event_id}")
    public ResponseEntity<String> closeEvent(@PathVariable int event_id) {
        return ResponseEntity.ok(eventService.closeEvent(event_id));
    }

    @ApiOperation(value="Get event by id")
    @GetMapping(path="/event/{id}")
    public ResponseEntity<EventDTO> getEventById(@PathVariable int id) {
        return ResponseEntity.ok(eventService.findById(id));
    }
}
