package com.arobs.meetups.Service.Vote;

import com.arobs.meetups.Entities.Vote;
import com.arobs.meetups.Repository.User.UserRepository;
import com.arobs.meetups.Repository.User.UserRepositoryConstants;
import com.arobs.meetups.Repository.User.UserRepositoryFactory;
import com.arobs.meetups.Repository.Votes.VoteRepository;
import com.arobs.meetups.Service.User.UserDTO;
import com.arobs.meetups.Service.User.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class VoteObject {
    @Autowired
    VoteRepository voteRepository;

    @Autowired
    VoteMapper voteMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserRepositoryFactory userRepositoryFactory;

    public void addVotes(VoteDTO voteDTO){
        if (voteDTO!=null){
            Vote vote = voteMapper.map(voteDTO, Vote.class);
            UserDTO userDTO = userMapper.map(vote.getUser(), UserDTO.class);
            voteRepository.addVotes(vote);
            awardVoter(userDTO.getUser_id());
        }
    }

    public int getVotesForProposal(int proposal_id){
        int voteCount = voteRepository.countVotesForProposal(proposal_id);
        if (voteCount > 0 ) {
            return voteCount;
        }
        return 0;
    }

    public List<VoteDTO> getAll() {
        List<Vote> voteList = voteRepository.getAll();
        return voteMapper.mapAsList(voteList, VoteDTO.class);
    }

    void awardVoter(int user_id){
        UserRepository userRepository = userRepositoryFactory.createUserRepository(UserRepositoryConstants.JDBC_REPOSITORY);
        userRepository.addPointsForUserID(1, user_id);
    }

    public List<VotedProposalsDTO> getTopProposals() {
        List<VoteDTO> voteDTOList = getAll(); // lista de (proposal_id, total_voturi)
        List<VotedProposalsDTO> votedProposalsDTOS = new ArrayList<>();

        for (VoteDTO voteDTO: voteDTOList) {
            VotedProposalsDTO votedProposalsDTO = new VotedProposalsDTO();
            int votes = getVotesForProposal(voteDTO.getProposal().getProposal_id());
            votedProposalsDTO.setProposalDTO(voteDTO.getProposal());
            votedProposalsDTO.setVotes(votes);
        }
        Collections.sort(votedProposalsDTOS);
        return votedProposalsDTOS;
    }
}
