package com.arobs.meetups.Service.Event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.beans.Transient;
import java.util.List;

@Component
public class EventServiceImpl implements EventService {
    @Autowired
    EventObject eventObject;

    @Override
    @Transactional
    public void addEvent(EventDTO eventDTO) {
        eventObject.addEvent(eventDTO);
    }

    @Override
    @Transactional
    public List<EventDTO> getAllEvents() {
        return eventObject.getAllEvents();
    }

    @Override
    @Transactional
    public void deleteEvent(int event_id) {
        eventObject.deleteEvent(event_id);
    }

    @Override
    @Transactional
    public void editEvent(EventDTO eventDTO) {
        eventObject.editEvent(eventDTO);
    }

    @Override
    @Transactional
    public int getMaxParticipants(int event_id) {
        return eventObject.getMaxParticipants(event_id);
    }

    @Override
    @Transactional
    public String closeEvent(int event_id) {
        return eventObject.closeEvent(event_id);
    }

    @Override
    @Transactional
    public EventDTO findById(int event_id) {
        return eventObject.findById(event_id);
    }

}
