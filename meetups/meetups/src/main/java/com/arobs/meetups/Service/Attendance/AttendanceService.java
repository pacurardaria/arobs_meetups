package com.arobs.meetups.Service.Attendance;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AttendanceService {
    void addAttendance(int event_id, int user_id);
    List<AttendanceDTO> getAllAttendance();
    void deleteAttendance(int attendance_id);
    void leaveFeedback(int attendance_id, String comment, int note);
}
