package com.arobs.meetups.Repository.Event;

import com.arobs.meetups.Entities.Event;
import com.arobs.meetups.Entities.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository {
    void addEvent(Event event);
    List<Event> getAllEvents();
    void deleteEvent(int event_id);
    void editEvent(Event event);
    int getMaxParticipants(int event_id);
    Event findById(int event_id);
    void closeEvent(int event_id);
}
