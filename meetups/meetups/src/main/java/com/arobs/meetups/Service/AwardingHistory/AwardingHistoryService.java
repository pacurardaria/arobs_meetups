package com.arobs.meetups.Service.AwardingHistory;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AwardingHistoryService {
    void addAwardingHistory(AwardingHistoryDTO awardingHistoryDTO);
    void editAwardingHistory(AwardingHistoryDTO awardingHistoryDTO);
    List<AwardingHistoryDTO> getAllAwardings();
    void deleteAwardingHistory(int awarding_id);
}
