package com.arobs.meetups.Service.Event;

import com.arobs.meetups.Entities.Event;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class EventMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Event.class, EventDTO.class)
                .byDefault()
                .register();
    }
}