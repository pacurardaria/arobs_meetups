package com.arobs.meetups.Service.Prize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class PrizeServiceImpl implements PrizeService {
    @Autowired
    PrizeObject prizeObject;

    @Override
    @Transactional
    public void addPrize(PrizeDTO prizeDTO) {
        prizeObject.addPrize(prizeDTO);
    }

    @Override
    @Transactional
    public void editPrize(PrizeDTO prizeDTO) {
        prizeObject.editPrize(prizeDTO);
    }

    @Override
    @Transactional
    public void deletePrize(int prize_id) {
        prizeObject.deletePrize(prize_id);
    }

    @Override
    @Transactional
    public List<PrizeDTO> getAllPrizes() {
        return prizeObject.getAllPrizes();
    }
}
