package com.arobs.meetups.Service.AwardingHistory;

import com.arobs.meetups.Entities.AwardingHistory;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class AwardingHistoryMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(AwardingHistory.class, AwardingHistoryDTO.class)
                .byDefault()
                .register();
    }
}
