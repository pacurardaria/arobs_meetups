package com.arobs.meetups.Service.Prize;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PrizeService {
    void addPrize(PrizeDTO prizeDTO);
    void editPrize(PrizeDTO prizeDTO);
    void deletePrize(int prize_id);
    List<PrizeDTO> getAllPrizes();
}
