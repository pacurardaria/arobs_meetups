package com.arobs.meetups.Repository.User;

import com.arobs.meetups.Entities.Event;
import com.arobs.meetups.Entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UserRepositoryHibernateImpl implements UserRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addUser(User user) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(user);
    }

    @Override
    public List<User> getAllUsers() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);
        cq.select(root);
        Query query = session.createQuery(cq);
        return query.getResultList();
    }

    @Override
    public void deleteUser(int user_id) {
        Session session = sessionFactory.getCurrentSession();
        User user = new User();
        user.setUser_id(user_id);
        session.delete(user);
    }

    @Override
    public void editUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.update(user);
    }

    @Override
    public void addPointsForUserID(int pointsToAdd, int user_id) {
    }

    @Override
    public User findById(int user_id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(User.class, user_id);
    }

    @Override
    public void clearPoints() {

    }


}
