package com.arobs.meetups.Service.AwardingHistory;

import java.sql.Date;

public class AwardingHistoryDTO {
    private int awarding_id;
    private Date awarding_date;
    private int number_of_points;

    public int getAwarding_id() {
        return awarding_id;
    }

    public void setAwarding_id(int awarding_id) {
        this.awarding_id = awarding_id;
    }

    public Date getAwarding_date() {
        return awarding_date;
    }

    public void setAwarding_date(Date awarding_date) {
        this.awarding_date = awarding_date;
    }

    public int getNumber_of_points() {
        return number_of_points;
    }

    public void setNumber_of_points(int number_of_points) {
        this.number_of_points = number_of_points;
    }
}
