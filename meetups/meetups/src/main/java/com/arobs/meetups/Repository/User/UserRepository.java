package com.arobs.meetups.Repository.User;

import com.arobs.meetups.Entities.User;

import java.util.List;

public interface UserRepository {
    void addUser(User user);
    List<User> getAllUsers();
    void deleteUser(int user_id);
    void editUser(User user);
    void addPointsForUserID(int pointsToAdd, int user_id);
    User findById(int user_id);
    void clearPoints();
}
