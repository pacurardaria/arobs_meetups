package com.arobs.meetups.Service.Prize;

import com.arobs.meetups.Entities.Prize;
import com.arobs.meetups.Entities.Proposal;
import com.arobs.meetups.Repository.Prize.PrizeRepository;
import com.arobs.meetups.Service.Proposal.ProposalDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PrizeObject {
    @Autowired
    PrizeRepository prizeRepository;

    @Autowired
    PrizeMapper prizeMapper;

    public List<PrizeDTO> getAllPrizes(){
        List<Prize> prizeList = prizeRepository.getAllPrizes();
        if (!prizeList.isEmpty()) {
            return prizeMapper.mapAsList(prizeList, PrizeDTO.class);
        }
        return null;
    }

    public void addPrize(PrizeDTO prizeDTO) {
        if (prizeDTO != null){
            Prize prize = prizeMapper.map(prizeDTO, Prize.class);
            prizeRepository.addPrize(prize);
        }
    }

    public void deletePrize(int prize_id) {
        prizeRepository.deletePrize(prize_id);
    }

    public void editPrize(PrizeDTO prizeDTO) {
        if (prizeDTO != null) {
            Prize prize = prizeMapper.map(prizeDTO, Prize.class);
            prizeRepository.editPrize(prize);
        }
    }
}
