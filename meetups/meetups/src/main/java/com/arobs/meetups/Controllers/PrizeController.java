package com.arobs.meetups.Controllers;

import com.arobs.meetups.Service.Prize.PrizeDTO;
import com.arobs.meetups.Service.Prize.PrizeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/prizes")
@Api(value = "Prizes controller")
public class PrizeController {
    @Autowired
    PrizeService prizeService;

    @ApiOperation(value = "Add prize")
    @PostMapping("/add")
    public ResponseEntity<PrizeDTO> addPrize(@RequestBody PrizeDTO  prizeDTO) {
        prizeService.addPrize(prizeDTO);
        return ResponseEntity.ok(prizeDTO);
    }

    @ApiOperation(value = "See all prizes")
    @GetMapping(path = "/all")
    public ResponseEntity<List<PrizeDTO>> getAll() {
        return ResponseEntity.ok(prizeService.getAllPrizes());
    }

    @ApiOperation(value = "Edit prize")
    @PutMapping("/edit")
    public ResponseEntity<PrizeDTO> editPrize(@RequestBody PrizeDTO prizeDTO) {
        prizeService.editPrize(prizeDTO);
        return ResponseEntity.ok(prizeDTO);
    }

    @ApiOperation(value = "Delete prize")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deletePrize(@PathVariable int prize_id) {
        prizeService.deletePrize(prize_id);
        return ResponseEntity.ok("Deleted");
    }

}
