package com.arobs.meetups.Service.Proposal;

import com.arobs.meetups.Entities.Proposal;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class ProposalMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Proposal.class, ProposalDTO.class)
                .byDefault()
                .register();
    }
}
