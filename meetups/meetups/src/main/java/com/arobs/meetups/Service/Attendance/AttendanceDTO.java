package com.arobs.meetups.Service.Attendance;

import com.arobs.meetups.Entities.Event;
import com.arobs.meetups.Entities.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
@JsonIgnoreProperties(ignoreUnknown = true)
public class AttendanceDTO {
    private int attendance_id;
    private User user;
    private Event event;

    private int note;

    private String comment;

    private Boolean isAwarded;

    public Boolean getAwarded() {
        return isAwarded;
    }

    public void setAwarded(Boolean awarded) {
        isAwarded = awarded;
    }

    public int getAttendance_id() {
        return attendance_id;
    }

    public void setAttendance_id(int attendance_id) {
        this.attendance_id = attendance_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
