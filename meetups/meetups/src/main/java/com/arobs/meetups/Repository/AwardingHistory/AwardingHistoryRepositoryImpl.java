package com.arobs.meetups.Repository.AwardingHistory;

import com.arobs.meetups.Entities.AwardingHistory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class AwardingHistoryRepositoryImpl implements AwardingHistoryRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addAwardingHistory(AwardingHistory awardingHistory) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(awardingHistory);
    }

    @Override
    public void editAwardingHistory(AwardingHistory awardingHistory) {
        Session session = sessionFactory.getCurrentSession();
        session.update(awardingHistory);
    }

    @Override
    public List<AwardingHistory> getAllAwardings() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<AwardingHistory> cq = cb.createQuery(AwardingHistory.class);
        Root<AwardingHistory> root = cq.from(AwardingHistory.class);
        cq.select(root);
        Query query = session.createQuery(cq);
        return query.getResultList();
    }

    @Override
    public void deleteAwardingHistory(int awarding_id) {
        Session session = sessionFactory.getCurrentSession();
        AwardingHistory  awardingHistory = new AwardingHistory();
        awardingHistory.setAwarding_id(awarding_id);
        session.delete(awarding_id);
    }
}
