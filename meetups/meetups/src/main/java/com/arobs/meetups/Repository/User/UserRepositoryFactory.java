package com.arobs.meetups.Repository.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserRepositoryFactory {
    @Autowired
    UserRepositoryJDBCImpl userRepositoryJDBC;

    @Autowired
    UserRepositoryHibernateImpl userRepositoryHibernate;

    public UserRepository createUserRepository(String type) {
        if (type.equals(UserRepositoryConstants.JDBC_REPOSITORY)) {
            return userRepositoryJDBC;
        } else if (type.equals(UserRepositoryConstants.HIBERNATE_REPOSITORY)) {
            return userRepositoryHibernate;
        }
        return null;
    }
}
