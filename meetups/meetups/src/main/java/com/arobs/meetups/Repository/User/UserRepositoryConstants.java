package com.arobs.meetups.Repository.User;

public final class UserRepositoryConstants {
    private UserRepositoryConstants() {}

    public static String JDBC_REPOSITORY = "JDBC";
    public static String HIBERNATE_REPOSITORY = "HIBERNATE";
}
