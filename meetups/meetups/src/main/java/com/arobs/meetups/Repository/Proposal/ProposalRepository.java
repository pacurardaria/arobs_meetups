package com.arobs.meetups.Repository.Proposal;

import com.arobs.meetups.Entities.Proposal;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProposalRepository {
    void addProposal(Proposal proposal);
    void deleteProposal(int proposal_id);
    void editProposal(Proposal proposal);
    List<Proposal> getAllProposals();
}
