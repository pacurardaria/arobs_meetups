package com.arobs.meetups.Service.Proposal;

public final class ProposalConstants {
    private ProposalConstants(){}

    public static String TECHNICAL_TYPE = "Technical";
    public static String PITCH_TYPE = "Pitch";
    public static String PRESENTATION_TYPE = "Presentation";
    public static String WORKSHOP_TYPE = "Workshop";

    public static String EASY_DIFFICULTY = "Easy";
    public static String MEDIUM_DIFFICULTY = "Medium";
    public static String HIGH_DIFFICULTY = "High";

    public static String EN_LANGUAGE = "English";
    public static String RO_LANGUAGE = "Romanian";
}
