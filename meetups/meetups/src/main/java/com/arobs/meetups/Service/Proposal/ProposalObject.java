package com.arobs.meetups.Service.Proposal;

import com.arobs.meetups.Entities.Proposal;
import com.arobs.meetups.Repository.Proposal.ProposalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProposalObject {
    @Autowired
    ProposalMapper proposalMapper;

    @Autowired
    ProposalRepository proposalRepository;

    public List<ProposalDTO> getAllProposals(){
        List<Proposal> proposalList = proposalRepository.getAllProposals();
        if (!proposalList.isEmpty()) {
            return proposalMapper.mapAsList(proposalList, ProposalDTO.class);
        }
        return null;
    }

    public void addProposal(ProposalDTO proposalDTO) {
        if (proposalDTO != null){
            Proposal proposal = proposalMapper.map(proposalDTO, Proposal.class);
            proposalRepository.addProposal(proposal);
        }
    }

    public void deleteProposal(int proposal_id) {
            proposalRepository.deleteProposal(proposal_id);
    }

    public void editProposal(ProposalDTO proposalDTO) {
        if (proposalDTO != null) {
            Proposal proposal = proposalMapper.map(proposalDTO, Proposal.class);
            proposalRepository.editProposal(proposal);
        }
    }
}
