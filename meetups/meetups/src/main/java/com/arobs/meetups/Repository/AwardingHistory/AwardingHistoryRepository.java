package com.arobs.meetups.Repository.AwardingHistory;

import com.arobs.meetups.Entities.AwardingHistory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AwardingHistoryRepository {
    void addAwardingHistory(AwardingHistory awardingHistory);
    void editAwardingHistory(AwardingHistory awardingHistory);
    List<AwardingHistory> getAllAwardings();
    void deleteAwardingHistory(int awarding_id);
}
