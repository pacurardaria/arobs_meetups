package com.arobs.meetups.Service.Attendance;

import com.arobs.meetups.Entities.Attendance;
import com.arobs.meetups.Entities.Event;
import com.arobs.meetups.Entities.User;
import com.arobs.meetups.Repository.Attendance.AttendanceRepository;
import com.arobs.meetups.Repository.Event.EventRepository;
import com.arobs.meetups.Repository.User.UserRepository;
import com.arobs.meetups.Repository.User.UserRepositoryConstants;
import com.arobs.meetups.Repository.User.UserRepositoryFactory;
import com.arobs.meetups.Service.User.UserDTO;
import com.arobs.meetups.Service.User.UserMapper;
import org.dom4j.util.UserDataDocumentFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Component
public class AttendanceObject {
    @Autowired
    AttendanceMapper attendanceMapper;

    @Autowired
    AttendanceRepository attendanceRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserRepositoryFactory userRepositoryFactory;



    public void addAttendance(int event_id, int user_id) {
        Event event = eventRepository.findById(event_id);
        UserRepository userRepository = userRepositoryFactory.createUserRepository(UserRepositoryConstants.HIBERNATE_REPOSITORY);
        User user = userRepository.findById(user_id);
        Attendance attendance = new Attendance();
        attendance.setUser(user);
        attendance.setEvent(event);
        attendance.setAwarded(false);
        if(event.getAttendees().size() <= event.getMax_participants()) {
            try {
                attendanceRepository.addAttendance(attendance);
            } catch (Exception e) {
                System.out.println("-----  User is already attending this event.");
            }
        }
    }

    public AttendanceDTO findById(int attendance_id) {
        Attendance attendance = attendanceRepository.findById(attendance_id);
        return attendanceMapper.map(attendance, AttendanceDTO.class);
    }

    public List<AttendanceDTO> getAllAttendance(){
        List<Attendance> attendanceList = attendanceRepository.getAllAttendance();
        if (!attendanceList.isEmpty()) {
            return attendanceMapper.mapAsList(attendanceList, AttendanceDTO.class);
        }
        return null;
    }

    public void deleteAttendance(int attendance_id) {
        attendanceRepository.deleteAttendance(attendance_id);

        //delete points for attendance from user
        AttendanceDTO attendanceDTO = findById(attendance_id);
        UserDTO userDTO = userMapper.map(attendanceDTO.getUser(), UserDTO.class);
        UserRepository userRepository = userRepositoryFactory.createUserRepository(UserRepositoryConstants.JDBC_REPOSITORY);
        userRepository.addPointsForUserID(-5, userDTO.getUser_id());

    }

    public void leaveFeedback(int attendance_id, String comment, int note) {
        UserRepository userRepository = userRepositoryFactory.createUserRepository(UserRepositoryConstants.HIBERNATE_REPOSITORY);
        Attendance attendance = attendanceRepository.findById(attendance_id);

        Timestamp currentDate = new Timestamp(System.currentTimeMillis());
        Event event = attendance.getEvent();
        User user = attendance.getUser();
        if(currentDate.after(Timestamp.valueOf(event.getDate()))) {
            attendanceRepository.leaveFeedback(attendance_id, note, comment);
            user.setPoints(user.getPoints() + 2);
            userRepository.editUser(user);
        }
    }

    @Transactional
    @Scheduled(fixedRate = 60 * 1000)
    @Async
    public void awardAttendee() {
        UserRepository userRepository = userRepositoryFactory.createUserRepository(UserRepositoryConstants.HIBERNATE_REPOSITORY);
        List<Attendance> attendances = attendanceRepository.getAllAttendance();

        for (Attendance attendance : attendances) {
            if (!attendance.getIsAwarded()) {
                User user = userRepository.findById(attendance.getUser().getUser_id());
                Event event = eventRepository.findById(attendance.getEvent().getEvent_id());
                if (event.isClosed()) {
                    user.setPoints(user.getPoints() + 5);
                    userRepository.editUser(user);
                    attendance.setAwarded(true);
                    attendanceRepository.awardUser(attendance);
                }
            }
        }
    }
}
