package com.arobs.meetups.Service.Vote;

import com.arobs.meetups.Service.Proposal.ProposalDTO;

public class VotedProposalsDTO implements Comparable<VotedProposalsDTO> {
    private ProposalDTO proposalDTO;
    private Integer votes;

    public ProposalDTO getProposalDTO() {
        return proposalDTO;
    }

    public void setProposalDTO(ProposalDTO proposalDTO) {
        this.proposalDTO = proposalDTO;
    }

    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    @Override
    public int compareTo(VotedProposalsDTO votedProposalsDTO) {
        return Integer.compare(votes, votedProposalsDTO.getVotes());
    }
}
