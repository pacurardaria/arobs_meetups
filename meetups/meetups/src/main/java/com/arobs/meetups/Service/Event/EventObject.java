package com.arobs.meetups.Service.Event;

import com.arobs.meetups.Entities.Event;
import com.arobs.meetups.Entities.User;
import com.arobs.meetups.Repository.Event.EventRepository;
import com.arobs.meetups.Repository.User.UserRepository;
import com.arobs.meetups.Repository.User.UserRepositoryConstants;
import com.arobs.meetups.Repository.User.UserRepositoryFactory;
import com.arobs.meetups.Service.User.UserDTO;
import com.arobs.meetups.Service.User.UserMapper;
import com.arobs.meetups.Service.User.UserObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EventObject {
    @Autowired
    EventMapper eventMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    UserObject userObject;

    @Autowired
    UserRepositoryFactory userRepositoryFactory;

    public void addEvent(EventDTO eventDTO) {
        if (eventDTO!=null){
            Event event = eventMapper.map(eventDTO, Event.class);
            eventRepository.addEvent(event);
        }
    }

    public List<EventDTO> getAllEvents(){
        List<Event> eventList = eventRepository.getAllEvents();
        if (!eventList.isEmpty()) {
            return eventMapper.mapAsList(eventList, EventDTO.class);
        }
        return null;
    }

    public void deleteEvent(int event_id) {
            eventRepository.deleteEvent(event_id);
    }

    public void editEvent(EventDTO eventDTO){
        if (eventDTO != null) {
            Event event = eventMapper.map(eventDTO, Event.class);
            eventRepository.editEvent(event);
        }
    }

    public EventDTO findById(int event_id) {
        Event event = eventRepository.findById(event_id);
        return eventMapper.map(event, EventDTO.class);
    }

    public int getMaxParticipants(int event_id){
        return eventRepository.getMaxParticipants(event_id);
    }

    public String closeEvent(int event_id) {
        try {
            EventDTO eventDTO = findById(event_id);
            if (!eventDTO.getClosed()) {
                UserDTO userDTO = userMapper.map(eventDTO.getUser(), UserDTO.class);
                awardOrganizer(userDTO.getUser_id(), eventDTO.getDifficulty());
                eventRepository.closeEvent(event_id);
                return "Event closed";
            }
            return "Event is already closed";
        } catch (Exception e) {
            return "Event doesn't exist.";
        }

    }

    public void awardOrganizer(int user_id, String difficulty) {
        UserRepository userRepository = userRepositoryFactory.createUserRepository(UserRepositoryConstants.JDBC_REPOSITORY);
        switch (difficulty) {
            case "Easy":
                userRepository.addPointsForUserID(20, user_id);
                break;
            case "Medium":
                userRepository.addPointsForUserID(35, user_id);
                break;
            case "Difficult":
                userRepository.addPointsForUserID(50, user_id);
                break;
        }
    }
}
