package com.arobs.meetups.Repository.Attendance;

import com.arobs.meetups.Entities.Attendance;
import com.arobs.meetups.Entities.Event;
import com.arobs.meetups.Entities.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttendanceRepository {
    Attendance findById(int attendance_id);
    void addAttendance(Attendance attendance);
    void leaveFeedback(int attendance_id, int note, String comment);
    List<Attendance> getAllAttendance();
    void deleteAttendance(int attendance_id);
    List<User> getAttendeesForEvent(int event_id);
    List<Event> getAttendedEventsForUser(int user_id);
    List<Attendance> getAttendeesToAward();
    void awardUser(Attendance attendance);
}
