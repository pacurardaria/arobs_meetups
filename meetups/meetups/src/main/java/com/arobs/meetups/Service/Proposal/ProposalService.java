package com.arobs.meetups.Service.Proposal;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProposalService {
    List<ProposalDTO> getAllProposals();
    void addProposal(ProposalDTO proposalDTO);
    void deleteProposal(int proposal_id);
    void editProposal(ProposalDTO proposalDTO);
}
