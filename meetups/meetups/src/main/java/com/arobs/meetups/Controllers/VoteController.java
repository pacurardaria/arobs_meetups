package com.arobs.meetups.Controllers;

import com.arobs.meetups.Service.Vote.VoteDTO;
import com.arobs.meetups.Service.Vote.VoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/votes")
@Api(value = "Votes controller")
public class VoteController {

    @Autowired
    private VoteService voteService;

    @ApiOperation(value = "Vote for proposal")
    @PostMapping("/add")
    public ResponseEntity addVote(@RequestBody VoteDTO  voteDTO) {
        try {
            voteService.addVotes(voteDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body("Successfully added vote");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bad request");

        }
    }

    @ApiOperation(value = "See all votes for proposals")
    @GetMapping(path = "/all")
    public ResponseEntity<List<VoteDTO>> getAll() {
        return ResponseEntity.ok(voteService.getAll());
    }

    @ApiOperation(value = "Get top voted proposals")
    @GetMapping(path = "/voted")
    public ResponseEntity getTop() {
        try {
            return ResponseEntity.ok(voteService.getTopProposals());
        } catch (Exception e) {
            return ResponseEntity.ok("Not found");
        }
    }



}
