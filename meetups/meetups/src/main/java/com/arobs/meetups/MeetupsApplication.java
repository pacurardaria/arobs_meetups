package com.arobs.meetups;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MeetupsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeetupsApplication.class, args);
	}

}
