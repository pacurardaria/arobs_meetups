package com.arobs.meetups.Repository.User;

import com.arobs.meetups.Entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepositoryJDBCImpl implements UserRepository{
    Logger logger = LoggerFactory.getLogger(UserRepository.class);

    @Autowired
    DataSource dataSource;

    public void addUser(User user) {
        String query = " insert into user(first_name, last_name, email, password, role)"
                + " values (?, ?, ?, ?, ?)";
        try(Connection connection = dataSource.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, user.getFirst_name());
            preparedStatement.setString(2, user.getLast_name());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setInt(5, user.getRole());
            preparedStatement.execute();
            logger.info("-----Added: " + user.getFirst_name() + " " + user.getLast_name() + "\n");

        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> getAllUsers() {
        String query = "select * from user";
        List<User> users = new ArrayList<>();
        try(Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            logger.info("From db: ");
            while(rs.next()) {
                User user = new User();
                user.setUser_id(rs.getInt("user_id"));
                user.setFirst_name(rs.getString("first_name"));
                user.setLast_name(rs.getString("last_name"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setRole(rs.getInt("role"));
                logger.info("-----" + user.getLast_name());
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public void deleteUser(int user_id) {

    }

    @Override
    public void editUser(User user) {

    }

    @Override
    public void addPointsForUserID(int pointsToAdd, int user_id) {
        String toExecute = "update user set points = points + ? where user_id = ?";
        try(Connection connection = dataSource.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(toExecute);
            preparedStatement.setInt(1, pointsToAdd);
            preparedStatement.setInt(2, user_id);
            preparedStatement.execute();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User findById(int user_id) {
        return null;
    }

    @Override
    public void clearPoints() {
        String toExecute = "update user set points = 0";
        try(Connection connection = dataSource.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(toExecute);
            preparedStatement.execute();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
