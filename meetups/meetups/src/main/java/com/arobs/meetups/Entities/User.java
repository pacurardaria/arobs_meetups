package com.arobs.meetups.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(
            strategy= GenerationType.AUTO,
            generator="native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private int user_id;
    private String first_name;
    private String last_name;
    private String email;
    private String password;
    private int role;
    private int points;

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
    @JsonIgnore
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL, fetch = FetchType.EAGER
    )
    Set<Attendance> eventsAttended = new HashSet<>();

    @JsonIgnore
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Proposal> proposalList = new ArrayList<>();

    @JsonIgnore
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Event> eventList = new ArrayList<>();

    @JsonIgnore
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Vote> voteList = new ArrayList<>();

    public void addVote(Vote vote) {
        voteList.add(vote);
        vote.setUser(this);
    }

    public void removeVote(Vote vote) {
        voteList.remove(vote);
        vote.setUser(null);
    }

    public void addProposal(Proposal proposal) {
        proposalList.add(proposal);
        proposal.setUser(this);
    }

    public void removeProposal(Proposal proposal) {
        proposalList.remove(proposal);
        proposal.setUser(null);
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public Set<Attendance> getEventsAttended() {
        return eventsAttended;
    }

    public void setEventsAttended(Set<Attendance> eventsAttended) {
        this.eventsAttended = eventsAttended;
    }

}
