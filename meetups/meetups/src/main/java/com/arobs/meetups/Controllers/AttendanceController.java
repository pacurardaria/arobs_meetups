package com.arobs.meetups.Controllers;

import com.arobs.meetups.Service.Attendance.AttendanceDTO;
import com.arobs.meetups.Service.Attendance.AttendanceService;
import com.arobs.meetups.Service.Event.EventDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/attendance")
@Api(value = "Attendance controller")
public class AttendanceController {
    @Autowired
    AttendanceService attendanceService;

    @ApiOperation(value = "Add attendance")
    @PostMapping("/add/{event_id}/{user_id}")
    public ResponseEntity<String> addAttendance(@PathVariable int event_id, @PathVariable int user_id) {
        attendanceService.addAttendance(event_id, user_id);
        return ResponseEntity.ok("Created attendance.");
    }

    @ApiOperation(value = "Delete attendance")
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<String> deleteAttendance(@PathVariable int id) {
        attendanceService.deleteAttendance(id);
        return ResponseEntity.ok("Deleted");
    }

    @ApiOperation(value = "See all attendance")
    @GetMapping(path = "/all")
    public ResponseEntity<List<AttendanceDTO>> getAll() {
        return ResponseEntity.ok(attendanceService.getAllAttendance());
    }

    @ApiOperation(value = "Leave feedback")
    @PutMapping("/edit/{attendance_id}/{comment}/{note}")
    public ResponseEntity<String> editEvent(@PathVariable int attendance_id, @PathVariable String comment, @PathVariable int note) {
        attendanceService.leaveFeedback(attendance_id, comment, note);
        return ResponseEntity.ok("Left feedback");
    }

}
