package com.arobs.meetups.Service.AwardingHistory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class AwardingHistoryServiceImpl implements AwardingHistoryService {
    @Autowired
    AwardingHistoryObject awardingHistoryObject;

    @Override
    @Transactional
    public void addAwardingHistory(AwardingHistoryDTO awardingHistoryDTO) {
        awardingHistoryObject.addAwardingHistory(awardingHistoryDTO);
    }

    @Override
    @Transactional
    public void editAwardingHistory(AwardingHistoryDTO awardingHistoryDTO) {
        awardingHistoryObject.editAwardingHistory(awardingHistoryDTO);
    }

    @Override
    @Transactional
    public List<AwardingHistoryDTO> getAllAwardings() {
        return awardingHistoryObject.getAllAwardings();
    }

    @Override
    @Transactional
    public void deleteAwardingHistory(int awarding_id) {
        awardingHistoryObject.deleteAwardingHistory(awarding_id);
    }
}
