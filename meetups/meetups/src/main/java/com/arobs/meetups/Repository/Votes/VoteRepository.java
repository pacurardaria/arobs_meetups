package com.arobs.meetups.Repository.Votes;

import com.arobs.meetups.Entities.Vote;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VoteRepository {
    void addVotes(Vote vote);
    int countVotesForProposal(int proposal_id);
    List<Vote> getAll();
}
