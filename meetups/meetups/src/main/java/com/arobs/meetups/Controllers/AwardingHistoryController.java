package com.arobs.meetups.Controllers;

import com.arobs.meetups.Service.AwardingHistory.AwardingHistoryDTO;
import com.arobs.meetups.Service.AwardingHistory.AwardingHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/awardings")
@Api(value = "Awarding history controller")
public class AwardingHistoryController {

    @Autowired
    AwardingHistoryService awardingHistoryService;

    @ApiOperation(value = "Add new awarding history")
    @PostMapping("/add")
    public ResponseEntity<AwardingHistoryDTO> addAwarding(@RequestBody AwardingHistoryDTO awardingHistoryDTO) {
        awardingHistoryService.addAwardingHistory(awardingHistoryDTO);
        return ResponseEntity.ok(awardingHistoryDTO);
    }

    @ApiOperation(value = "Edit awarding history")
    @PutMapping("/edit")
    public ResponseEntity<AwardingHistoryDTO> editAwardingHistory(@RequestBody AwardingHistoryDTO awardingHistoryDTO) {
        awardingHistoryService.editAwardingHistory(awardingHistoryDTO);
        return ResponseEntity.ok(awardingHistoryDTO);
    }

    @ApiOperation(value = "Delete awarding")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteEvent(@PathVariable int id) {
        awardingHistoryService.deleteAwardingHistory(id);
        return ResponseEntity.ok("Deleted");
    }

    @ApiOperation(value = "See all awardings")
    @GetMapping(path = "/all")
    public ResponseEntity<List<AwardingHistoryDTO>> getAll() {
        return ResponseEntity.ok(awardingHistoryService.getAllAwardings());
    }

}
