package com.arobs.meetups.Service.Proposal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class ProposalServiceImpl implements ProposalService {
    @Autowired
    ProposalObject proposalObject;

    @Override
    @Transactional
    public List<ProposalDTO> getAllProposals() {
        return proposalObject.getAllProposals();
    }

    @Override
    @Transactional
    public void addProposal(ProposalDTO proposalDTO) {
        proposalObject.addProposal(proposalDTO);
    }

    @Override
    @Transactional
    public void deleteProposal(int proposal_id) {
        proposalObject.deleteProposal(proposal_id);
    }

    @Override
    @Transactional
    public void editProposal(ProposalDTO proposalDTO) {
        proposalObject.editProposal(proposalDTO);
    }
}
