package com.arobs.meetups.Service.AwardingHistory;

import com.arobs.meetups.Entities.AwardingHistory;
import com.arobs.meetups.Repository.AwardingHistory.AwardingHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AwardingHistoryObject {
    @Autowired
    AwardingHistoryMapper awardingHistoryMapper;

    @Autowired
    AwardingHistoryRepository awardingHistoryRepository;


    public void addAwardingHistory(AwardingHistoryDTO awardingHistoryDTO) {
        if (awardingHistoryDTO!=null){
            AwardingHistory awardingHistory = awardingHistoryMapper.map(awardingHistoryDTO, AwardingHistory.class);
            awardingHistoryRepository.addAwardingHistory(awardingHistory);
        }
    }

    public List<AwardingHistoryDTO> getAllAwardings(){
        List<AwardingHistory> awardingHistoryList = awardingHistoryRepository.getAllAwardings();
        if (!awardingHistoryList.isEmpty()) {
            return awardingHistoryMapper.mapAsList(awardingHistoryList, AwardingHistoryDTO.class);
        }
        return null;
    }

    public void deleteAwardingHistory(int awarding_id) {
        awardingHistoryRepository.deleteAwardingHistory(awarding_id);
    }

    public void editAwardingHistory(AwardingHistoryDTO awardingHistoryDTO){
        if (awardingHistoryDTO != null) {
            AwardingHistory awardingHistory = awardingHistoryMapper.map(awardingHistoryDTO, AwardingHistory.class);
            awardingHistoryRepository.editAwardingHistory(awardingHistory);
        }
    }
}
