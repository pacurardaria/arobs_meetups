package com.arobs.meetups.Service.Vote;

import com.arobs.meetups.Entities.User;
import com.arobs.meetups.Service.Proposal.ProposalDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
@JsonIgnoreProperties(ignoreUnknown = true)
public class VoteDTO implements Comparable<VoteDTO> {
    private int vote_id;
    private User user;
    private ProposalDTO proposal;

    public int getVote_id() {
        return vote_id;
    }

    public void setVote_id(int vote_id) {
        this.vote_id = vote_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ProposalDTO getProposal() {
        return proposal;
    }

    public void setProposal(ProposalDTO proposal) {
        this.proposal = proposal;
    }

    @Override
    public int compareTo(VoteDTO vote) {
        return this.getProposal().compareTo(vote.getProposal());
    }
}
