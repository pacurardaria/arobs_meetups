package com.arobs.meetups.Repository.Attendance;

import com.arobs.meetups.Entities.Attendance;
import com.arobs.meetups.Entities.Event;
import com.arobs.meetups.Entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class AttendanceRepositoryImpl implements AttendanceRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Attendance findById(int attendance_id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Attendance.class, attendance_id);
    }

    @Override
    public void addAttendance(Attendance attendance) {
        Session session = sessionFactory.getCurrentSession();
        session.save(attendance);
    }

    @Override
    public void leaveFeedback(int attendance_id, int note, String comment) {
        Session session = sessionFactory.getCurrentSession();
        String queryString = "update attendance  " +
                " set attendance.comment = ?, " +
                " attendance.note = ?," +
                " where attendance.attendance_id = ? ";
        Query query = session.createNativeQuery(queryString);
        query.setParameter(1, comment);
        query.setParameter(2, note);
        query.setParameter(3, attendance_id);
        query.executeUpdate();
    }

    @Override
    public List<Attendance> getAllAttendance() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Attendance> cq = cb.createQuery(Attendance.class);
        Root<Attendance> root = cq.from(Attendance.class);
        cq.select(root);
        Query query = session.createQuery(cq);
        return query.getResultList();
    }

    @Override
    public void deleteAttendance(int attendance_id) {
        Session session = sessionFactory.getCurrentSession();
        Attendance attendance = new Attendance();
        attendance.setAttendance_id(attendance_id);
        session.delete(attendance);
    }

    @Override
    public List<User> getAttendeesForEvent(int event_id) {
        Session session = sessionFactory.getCurrentSession();
        return session.createNativeQuery("SELECT * FROM user INNER JOIN attendance ON attendance.user_id = user.user_id WHERE attendance.event_id =?")
                .addEntity(User.class)
                .setParameter(1, event_id).list();
    }

    @Override
    public List<Event> getAttendedEventsForUser(int user_id) {
        Session session = sessionFactory.getCurrentSession();
        return session.createNativeQuery("SELECT * FROM event INNER JOIN attendance ON attendance.event_id = event.event_id WHERE attendance.user_id = ?")
                .addEntity(User.class)
                .setParameter(1, user_id).list();
    }

    @Override
    public List<Attendance> getAttendeesToAward() {
        Session session = sessionFactory.getCurrentSession();
        String queryString = "select a.* " +
                " from user u , attendance a, event e " +
                " where u.user_id = a.user_id " +
                " and a.event_id = e.event_id " +
                " and e.isClosed = 1 and a.isAwarded = 0;";
        Query query = session.createNativeQuery(queryString);
        return query.getResultList();
    }

    @Override
    public void awardUser(Attendance attendance) {
        Session session = sessionFactory.getCurrentSession();
        session.update(attendance);
    }
}
