package com.arobs.meetups.Service.Attendance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class AttendanceServiceImpl implements AttendanceService {

    @Autowired
    AttendanceObject attendanceObject;

    @Override
    @Transactional
    public void addAttendance(int event_id, int user_id) {
        attendanceObject.addAttendance(event_id, user_id);
    }

    @Override
    @Transactional
    public List<AttendanceDTO> getAllAttendance() {
        return attendanceObject.getAllAttendance();

    }

    @Override
    @Transactional
    public void deleteAttendance(int attendance_id) {
        attendanceObject.deleteAttendance(attendance_id);

    }

    @Override
    @Transactional
    public void leaveFeedback(int attendance_id, String comment, int note) {
        attendanceObject.leaveFeedback(attendance_id, comment, note);
    }
}
