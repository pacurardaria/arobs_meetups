package com.arobs.meetups.Service.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    UserObject userObject;

    @Override
    @Transactional
    public void addUser(UserDTO u) {
        userObject.saveUser(u);
    }

    @Override
    @Transactional
    public List<UserDTO> getAllUsers() {
        return userObject.getAllUsers();
    }

    @Override
    @Transactional
    public void deleteUser(int user_id) {
        userObject.deleteUser(user_id);
    }

    @Override
    @Transactional
    public void editUser(UserDTO userDTO) {
        userObject.editUser(userDTO);
    }

    @Override
    @Transactional
    public void addPointsForUserID(int points, int user_id) {
        userObject.addPointsForUserID(points, user_id);
    }

    @Override
    @Transactional
    public void clearPoints() {
        userObject.clearPoints();
    }

}
