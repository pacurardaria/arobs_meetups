package com.arobs.meetups.Repository.Event;

import com.arobs.meetups.Entities.Event;
import com.arobs.meetups.Entities.User;
import com.arobs.meetups.Repository.User.UserRepository;
import com.arobs.meetups.Repository.User.UserRepositoryConstants;
import com.arobs.meetups.Repository.User.UserRepositoryFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class EventRepositoryImpl implements EventRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    UserRepositoryFactory userRepositoryFactory;

    @Override
    public void addEvent(Event event) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(event);
    }

    @Override
    public List<Event> getAllEvents() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Event> cq = cb.createQuery(Event.class);
        Root<Event> root = cq.from(Event.class);
        cq.select(root);
        Query query = session.createQuery(cq);
        return query.getResultList();
    }

    @Override
    public void deleteEvent(int event_id) {
        Session session = sessionFactory.getCurrentSession();
        Event event = new Event();
        event.setEvent_id(event_id);
        session.delete(event);
    }

    @Override
    public void editEvent(Event event) {
        Session session = sessionFactory.getCurrentSession();
        session.update(event);
    }

    @Override
    public int getMaxParticipants(int event_id) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Event> cq = cb.createQuery(Event.class);
        Root<Event> root = cq.from(Event.class);
        cq.select(root.get("max_participants")).where(cb.equal(root.get("event_id"), event_id));
        Query query = session.createQuery(cq);
        return query.getFirstResult();
    }

    @Override
    public Event findById(int event_id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Event.class, event_id);
    }

    @Override
    public void closeEvent(int event_id) {
        Session session = sessionFactory.getCurrentSession();
        String queryString = "update event  " +
                " set event.isClosed = 1 " +
                " where event.event_id = ? ";
        Query query = session.createNativeQuery(queryString);

        query.setParameter(1, event_id);
        query.executeUpdate();
    }

}
