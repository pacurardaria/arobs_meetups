package com.arobs.meetups.Repository.Prize;

import com.arobs.meetups.Entities.Prize;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrizeRepository {
    void addPrize(Prize prize);
    void editPrize(Prize prize);
    void deletePrize(int prize_id);
    List<Prize> getAllPrizes();
}
